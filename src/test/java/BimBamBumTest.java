import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BimBamBumTest {
    protected BimBamBum bimBamBum = new BimBamBum();

    @Test
    public void bimBamBumNormalNumbers() {
        Assertions.assertEquals("1", bimBamBum.convert(1));
        Assertions.assertEquals("2", bimBamBum.convert(2));
    }

    @Test
    public void bimBamBumDivisibleByThree() {
        Assertions.assertEquals("Bim", bimBamBum.convert(3));
    }

    @Test
    public void bimBamBumDivisibleByFive() {
        Assertions.assertEquals("Bam", bimBamBum.convert(5));
    }

    @Test
    public void bimBamBumDivisibleBySeven() {
        Assertions.assertEquals("Bum", bimBamBum.convert(7));
    }

    @Test
    public void bimBamBumDivisibleByThreeAndFive() {
        Assertions.assertEquals("BimBam", bimBamBum.convert(15));
    }

    @Test
    public void bimBamBumDivisibleByThreeAndSeven() {
        Assertions.assertEquals("BimBum", bimBamBum.convert(21));
    }

    @Test
    public void bimBamBumDivisibleByFiveAndSeven() {
        Assertions.assertEquals("BamBum", bimBamBum.convert(35));
    }

    @Test
    public void bimBamBumDivisibleByThreeAndFiveAndSeven() {
        Assertions.assertEquals("BimBamBum", bimBamBum.convert(105));
    }

    @Test
    public void bimBamBumPrintAllNumbers() {
        for (int i = 1; i<=1000; i++) {
            System.out.println(bimBamBum.convert(i));
        }
    }
}
