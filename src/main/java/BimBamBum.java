public class BimBamBum {
    public String convert(int toConvert) {

        if ((toConvert % 3 == 0) && (toConvert % 5 == 0) && (toConvert % 7 == 0)) {
            return "BimBamBum";
        }
        else if ((toConvert % 5 == 0) && (toConvert % 7 == 0)) {
            return "BamBum";
        }
        else if ((toConvert % 3 == 0) && (toConvert % 7 == 0)) {
            return "BimBum";
        }
        else if ((toConvert % 3 == 0) && (toConvert % 5 == 0)) {
            return "BimBam";
        }
        else if (toConvert % 3 == 0) {
            return "Bim";
        }
        else if (toConvert % 5 == 0) {
            return "Bam";
        }
        else if (toConvert % 7 == 0) {
            return "Bum";
        }

        return String.valueOf(toConvert);
    }
}
